% !TEX TS-program = xelatex

\documentclass[a4paper]{article}

%%% PACKAGES %%%
\usepackage[a4paper]{geometry} % margins
\usepackage{datetime2} % YYYY-MM-DD date format
\usepackage{fancyhdr} % header & footer
\usepackage{float} % place figures where desired
\usepackage{fontspec} % custom fonts
\usepackage{graphicx} % images
\usepackage{hyperref} % hyperlinks
\usepackage{paralist} % compact lists
\usepackage{xcolor} % coloured text

%%% SETTINGS %%%
\setmainfont{Junicode}
\setmonofont{Inconsolata}
\pagestyle{fancyplain}
\fancyhf{}
\chead{jvtris}
\rhead{@bertalanp99}
\rfoot{\thepage}

%%% MACROS %%%
\newcommand\csample[1]{\textcolor{#1}{\rule{1em}{1em}}}
\def\jvtris/{\textit{jvtris}}
\def\tetrI/{\textbf{I}}
\def\tetrJ/{\textbf{J}}
\def\tetrL/{\textbf{L}}
\def\tetrO/{\textbf{O}}
\def\tetrS/{\textbf{S}}
\def\tetrZ/{\textbf{Z}}
\def\tetrT/{\textbf{T}}

%%% META %%%
\title{jvtris \\ \large a Java Tetris clone}
\date{\today}
\author{@bertalanp99}

\begin{document}

\maketitle

\section{Short summary}

\href{https://en.wikipedia.org/wiki/Tetris}{Tetris} is quite a well-known game.
According to Wikipedia, this genre is called
\href{https://en.wikipedia.org/wiki/Tile-matching_video_game}{tile-matching
video game}.

\jvtris/ aims to provide a similar experience to the original game --- i.e.\
there is a rectangular playfield and a fixed number of different shapes (from
now on, \emph{\href{https://en.wikipedia.org/wiki/Tetromino}{tetrominoes}})
`fall' from the top; the task of the player is to place these tetrominoes in
such a way that lines full of blocks are formed: when this happens, the line is
cleared and the player is awarded points. To achieve this, the player may move
the tetromino left and right on the playfield. Additionally, tetrominoes may be
rotated (by 90 degrees each time) and dropped (basically speeding up their
descent until they hit the bottom). The player can clear several lines at once:
a special case of this is clearing four lines, which is called a \emph{Tetris}.
When a line is cleared, all
\href{https://en.wikipedia.org/wiki/Polyomino}{polyominoes} above it are shifted
down by one.

As the player advances, the game \emph{level} increases. Increased level
involves increasing the pace of the game (i.e.\ tetrominoes fall more quickly,
thus the player has less time to decide). The game has no end goal, so it can
virtually be played forever, although if an uncorrelated random number generator
is used for randomizing the chain of falling tetrominoes, this can be proven to
be impossible (cf Wikipedia).

\section{Use cases}

\begin{minipage}{.5\textwidth}
	The player may

	\begin{compactitem}
	\item start a new game
	\item view highscores
	\item change options
	\end{compactitem}
\end{minipage}
%
\begin{minipage}{.5\textwidth}
	\begin{figure}[H]
		\centering
		\includegraphics[width=.6\textwidth]{../diagrams/usecase/player.png}
		\caption{Use case diagram}
	\end{figure}
\end{minipage}

\section{Implementation plan}

\subsection{Clarifications}

Tetris has many variants, which may have different rule sets, score systems or
even new (or missing) tetrominoes. Hereby such attributes are specified.

\begin{itemize}
	\item the playfield dimensions are \(10 \times 20\)
	\item the following \(7\) (named) tetrominoes exist:
\begin{verbatim}
+---+---+---+---+                   +---+---+
|   |   |   |   |  (I)              |   |   |
+---+---+---+---+               +-----------+    (S)
                                |   |   |
  +---+                         +---+---+
  |   |
  +-------+---+    (J)          +---+---+
  |   |   |   |                 |   |   |
  +---+---+---+                 +-----------+    (Z)
                                    |   |   |
          +---+                     +---+---+
          |   |
  +---+-------+    (L)              +---+
  |   |   |   |                     |   |
  +---+---+---+                 +-----------+    (T)
                                |   |   |   |
    +---+---+                   +---+---+---+
    |   |   |
    +-------+      (O)
    |   |   |
    +---+---+
\end{verbatim}
	\item once dropped, tetrominoes may not be rotated, but can still be moved
		(until the next tetromino enters the playfield)
	\item there are no `chain reactions' (i.e.\  no gravity-like force applies
		to the tetrominoes: they may float after line clears)
	\item scoring:
		\begin{compactitem}
		\item clearing a line awards the player with \(100\) points
		\item clearing a \emph{Tetris} awards the player with \(800\) points
		\item each subsequent back-to-back \emph{Tetris} is worth \(1200\)
			points
		\item there is no other way to earn points
		\end{compactitem}
	\item tetromino colours are in accordance to
		\href{https://en.wikipedia.org/wiki/The_Tetris_Company}{The Tetris
		Company's} standardization:
		\begin{figure}[H]
			\centering
			\begin{tabular}{ccc}
				\tetrI/	& cyan		& \csample{cyan}	\\
				\tetrJ/	& blue		& \csample{blue}	\\
				\tetrL/	& orange	& \csample{orange}	\\
				\tetrO/	& yellow	& \csample{yellow}	\\
				\tetrS/	& green		& \csample{green}	\\
				\tetrZ/	& red		& \csample{red}		\\
				\tetrT/	& magenta	& \csample{magenta}	\\
			\end{tabular}
			\caption{table of tetromino colours}
		\end{figure}
	\item the next-tetromino algorithm is dumb: it is simply based on an
		uncorrelated random number generator
	\item the player advances to the next level with every \(10\) lines cleared
	\item the leaderboards keep track of the \(10\) best players
\end{itemize}

\subsection{Plan}

Looking at the game mechanics (being block-based), it seems to be a sane choice
to realise the playfield as a \emph{2-dimensional array} (basically a matrix).
The elements of this matrix are preferably \texttt{char}s that are corresponding
to the tetromino names. This makes it easy to colourize the display and keep
track of tetrominoes even after having been placed. \textit{(Note that this is
only necessary because jvtris is a coloured version; without colours, we would
not care about what block belongs to which tetromino)}

All tetrmonioes fit into a \(2 \times 4\) matrix, so it seems logical to store
them that way. However, rotated tetrominoes also need to be represented and
since there are only \(4\) rotations posibble for each tetromino, all different
rotations could be prestored. This way (reasonably) complicated rotation
algorithms can be avoided. To fit all tetromino orientations, \(4 \times 4\)
matrices shall be used.

Some sort of timing needs to be implemented as well. There could be \emph{ticks}
whose pace increases with the player's level. At each \emph{tick}, the currently
active tetromino needs to move down by one; naturally, collision with other
tetrominoes and the playfield boundaries needs to be checked.

After a tetromino is finalized (i.e.\ just before the next tetromino enters the
playfield), the game should check for full lines and clear accordingly.

The game needs to feature persistence: the leaderboards should persist through
runs. This can be achieved by means of simple serialization.

\end{document}
