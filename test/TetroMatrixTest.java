package eu.bertalanp99.jvtris;

import org.junit.Test;

class TetroMatrixTest {
	@Test(expected=IllegalArgumentException.class)
	public void testCheck2DByteArrValidity()
	{
		byte[][] mat = new byte[][]
		{
			{1, 0, 1, 1},
			{0, 0, 1, 1},
			{1, 1, 1, 1}
		};
		TetroMatrix foo = new TetroMatrix(4, mat);
	}
}
