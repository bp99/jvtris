package eu.bertalanp99.jvtris;

import java.util.Arrays;

/**
 * Array wrapper class with extra features for use in jvtris exclusively.
 *
 * @version 0.1
 * @since 0.1
 * @author bertalanp99
 */
class TetroArray {
	/**
	 * The array storing the data of the <code>TetroArray</code>.
	 */
	private final byte[] array;


	/**
	 * The width of the <code>PlayField</code> this <code>TetroArray</code> is
	 * designed for.
	 */
	private final int playFieldWidth;

	/**
	 * Constructs a <code>TetroArray</code> and fills its contents with the
	 * given <code>byte</code> array.
	 *
	 * In case the <code>byte</code> array's width is less than
	 * <code>width</code>, the constructor automatically expands it (padding
	 * with <code>0</code>s from both sides) in such a way that the contents of
	 * the original array will get centre-aligned within the expanded one.
	 *
	 * @param width the width of the <code>PlayField</code> the
	 * <code>TetroArray</code> is designed for
	 * @param arr the <code>byte</code> array to fill the
	 * <code>TetroArray</code> with
	 *
	 * @see #expandToWidth(byte[])
	 */
	public TetroArray(int width, byte[] arr)
	{
		if (arr == null)
		{
			throw new NullPointerException();
		}
		else if (width < arr.length)
		{
			throw new IllegalArgumentException("Playfield width must be " +
					"at least array length.");
		}

		this.playFieldWidth = width;


		if (arr.length != this.playFieldWidth)
		{
			arr = this.expandToWidth(arr);
		}

		this.array = Arrays.copyOf(arr, this.playFieldWidth);
	}

	/**
	 * Constructs an empty <code>TetroArray</code>.
	 *
	 * Delegates {@link #TetroArray(int, byte[])} with a newly initialized
	 * <code>byte</code> array.
	 *
	 * @param width the width of the <code>PlayField</code> the
	 * <code>TetroArray</code> is designed for
	 */
	public TetroArray(int width)
	{
		this(width, new byte[width]);
	}

	/**
	 * Constructs a <code>TetroArray</code> from another (copy constuctor).
	 *
	 * Used to effectively clone a <code>TetroArray</code>.
	 *
	 * @param src <code>TetroArray</code> to copy from
	 */
	public TetroArray(TetroArray src)
	{
		this.array = Arrays.copyOf(src.array, src.playFieldWidth);
		this.playFieldWidth = src.playFieldWidth;
	}
	
	/**
	 * Returns the width of the <code>PlayField</code> this
	 * <code>TetroArray</code> is designed for.
	 *
	 * @return the width of the <code>PlayField</code>
	 */
	public int size()
	{
		return this.playFieldWidth;
	}

	/**
	 * Returns the <code>n</code>-th element of this <code>TetroArray</code>.
	 *
	 * This effectively indexes the inner array.
	 *
	 * @param n the element index
	 *
	 * @return the element at index <code>n</code>
	 */
	public byte getNth(int n)
	{
		if (n < 0 || n > this.playFieldWidth)
		{
			throw new IndexOutOfBoundsException("Index out of bounds (should " +
					" be at least 0 and less than playfield width).");
		}

		return this.array[n];
	}

	/**
	 * Expands a <code>byte</code> array to be as long as the
	 * <code>PlayField</code>'s width this <code>TetroArray</code> is designed
	 * for.
	 *
	 * The resulting array is padded from both directions to achieve expansion
	 * in such a way that the original array is the most centered in the
	 * resulting array. When the array cannot be properly centered, the first
	 * (left) possible positioning is used.
	 *
	 * @param arr the <code>byte</code> array to exapdn
	 *
	 * @return the expanded array
	 */
	private byte[] expandToWidth(byte[] arr)
	{
		if (arr == null)
		{
			throw new NullPointerException();
		}

		byte[] rv = new byte[this.playFieldWidth];

		/* Examples for counting the (left) padding for center-alignment:
		 *
		 * original array (odd): <x> <x> <x> (length is 3)
		 * width (odd): 7
		 * ==> <0> <0> <x> <x> <x> <0> <0>
		 * ==> ==> padding is 2
		 *
		 * original array (odd): <x> <x> <x> (length is 3)
		 * width (even): 8
		 * ==> <0> <0> <x> <x> <x> <0> <0> <0>
		 * ==> ==> padding is 2
		 *
		 * original array (even): <x> <x> <x> <x> (length is 4)
		 * width (odd): 7
		 * ==> <0> <x> <x> <x> <x> <0> <0>
		 * ==> ==> padding is 1
		 *
		 * original array (even): <x> <x> <x> <x> (length is 4)
		 * width (even): 8
		 * ==> <0> <0> <x> <x> <x> <x> <0> <0>
		 * ==> ==> padding is 2
		 *
		 * From this it seems that the following formula can be used to
		 * calculate the padding:
		 *
		 * P = floor( (W - (ceil(W / 2) - floor(L / 2))) / 2 )
		 *
		 * where
		 *  P is the padding,
		 *  W is the width of the padded array, and
		 *  L is the length of the original array
		 *
		 */
		int padding = (
				(int) Math.floor(
					(
						this.playFieldWidth -
						(
							Math.ceil(this.playFieldWidth / 2) -
							Math.floor(arr.length / 2)
						)
					) / 2
				)
		);

		int i = 0;
		byte padder = 0;
		while (i < padding)
		{
			rv[i++] = padder;
		}
		for (byte b : arr)
		{
			rv[i++] = b;
		}
		while (i < rv.length)
		{
			rv[i++] = padder;
		}

		return rv;
	}

	/**
	 * Returns a left-shifted version of this <code>TetroArray</code>.
	 *
	 * Here is a visualization of what shifting left means:
	 *  <code>[0 1 1 0 1 0] becomes [1 1 0 1 0 0]</code>
	 *  (the leftmost <code>0</code> in the orinal array is discarded)
	 *
	 * The method always pads the extra space with <code>0</code>s.
	 *
	 * @param amount how much to shift left by
	 *
	 * @return the resulting <code>TetroArray</code> after the shift
	 *
	 * @see #shift(int)
	 */
	public TetroArray shiftLeft(int amount)
	{
		if (amount < 0)
		{
			throw new IllegalArgumentException("Shift amount must be positive");
		}

		if (amount == 0)
		{
			return new TetroArray(this);
		}

		// make sure only 0s would be discarded, otherwise give null
		if (this.array[amount - 1] != 0)
		{
			return null;
		}

		TetroArray rv = new TetroArray(this);

		byte padder = 0;
		for (int i = 0; i < rv.array.length; ++i)
		{
			if((i + amount) >= this.playFieldWidth)
			{
				rv.array[i] = padder;
			}
			else
			{
				rv.array[i] = this.array[i + amount];
			}
		}

		return rv;
	}

	/**
	 * Shorter version of {@link #shiftLeft()} that shifts by <code>1</code>
	 * only.
	 *
	 * @return the result of @{@link $shiftRight()}
	 */
	public TetroArray shiftLeft()
    {
		return this.shiftLeft(1);
	}

	/**
	 * Returns a right-shifted version of this <code>TetroArray</code>.
	 *
	 * Here is a visualization of what shifting right means:
	 *  <code>[1 1 1 0 0 1] becomes [0 1 1 1 0 0]</code>
	 *  (the rightmost <code>1</code> in the original array is discarded)
	 *
	 * The method always pads the extra space with <code>0</code>s.
	 *
	 * @param amount how much to shift right by
	 *
	 * @return the resulting <code>TetroArray</code> after the shift
	 *
	 * @see #shift(int)
	 */
	public TetroArray shiftRight(int amount)
	{
		if (amount < 0)
		{
			throw new IllegalArgumentException("Shift amount must be positive");
		}

		if (amount == 0)
		{
			return new TetroArray(this);
		}

		// make sure only 0s would be discarded, otherwise give null
		if (this.array[this.playFieldWidth - amount] != 0)
		{
			return null;
		}

		TetroArray rv = new TetroArray(this);

		byte padder = 0;
		for (int i = 0; i < rv.array.length; ++i)
		{
			if((i - amount) < 0)
			{
				rv.array[i] = padder;
			}
			else
			{
				rv.array[i] = this.array[i - amount];
			}
		}

		return rv;
	}

	/**
	 * Shorter version of {@link #shiftRight()} that shifts by <code>1</code>
	 * only.
	 *
	 * @return the result of {@link #shiftRight()}
	 */
	public TetroArray shiftRight()
	{
		return this.shiftRight(1);
	}

	/**
	 * Returns a shifted version of this <code>TetroArray</code>.
	 *
	 * This method simply delegates to {@link #shiftLeft(int)} and {@link
	 * $shiftRight(int)}, based on the sign of <code>amount</code>.
	 *
	 * @param amount for negative numbers, shift <em>left</em> by this, for
	 * positive ones, shift <em>right</em>
	 *
	 * @return the resulting <code>TetroArray</code> after the shift
	 *
	 * @see #shiftLeft(int)
	 * @see #shiftRight(int)
	 */
	public TetroArray shift(int amount)
	{
		if (amount < 0)
		{
			return this.shiftLeft(-amount);
		}
		else if (amount > 0)
		{
			return this.shiftRight(amount);
		}

		return this;
	}

	/**
	 * Decides whether the TetroArray has a zero on the left (the first
	 * element).
	 *
	 * @return <code>true</code> if the first element is <code>0</code>,
	 * <code>false</code> otherwise.
	 */
	public boolean isLeftPadded()
	{
		return (this.array[0] == 0);
	}

	/**
	 * Decides whether the TetroArray has a zero on the right (the last
	 * element).
	 *
	 * @return <code>true</code> if the last element is <code>0</code>,
	 * <code>false</code> otherwise.
	 */
	public boolean isRightPadded()
	{
		return (this.array[this.playFieldWidth - 1] == 0);
	}

	/**
	 * Decides whether the TetroArray could be merged without conflict with
	 * another TetroArray.
	 *
	 * Pretty much performs a bitwise operation.
	 *
	 * @param ta the TetroArray to check mergeability with
	 *
	 * @return <code>true</code> if merging is possible, <code>false</code>
	 * otherwise
	 */
	public boolean isMergeableWith(TetroArray ta)
	{
		if (this.playFieldWidth != ta.playFieldWidth)
		{
			throw new IllegalArgumentException("This method can only be " +
					"applied to tetromino arrays created for the same" +
					"playfield size.");
		}

		for (int i = 0; i < this.playFieldWidth; ++i)
		{
			if (this.array[i] != 0 && ta.array[i] != 0)
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * Merges the TetroArray with another one.
	 *
	 * @param ta the TetroArray to merge with
	 *
	 * @return the TetroArray resulting after the merge
	 */
	public TetroArray mergeWith(TetroArray ta)
	{
		if (this.playFieldWidth != ta.playFieldWidth)
		{
			throw new IllegalArgumentException("This method can only be " +
					"applied to tetromino arrays created for the same" +
					"playfield size.");
		}

		TetroArray rv = new TetroArray(this);

		for (int i = 0; i < rv.playFieldWidth; ++i)
		{
			rv.array[i] = (byte) (this.array[i] | ta.array[i]);
		}

		return rv;
	}

	/**
	 * Returns whether the TetroArray is all <code>0</code>s.
	 *
	 * @return <code>true</code> if all elements are equal to <code>0</code>,
	 * <code>false</code> otherwise
	 */
	public boolean isZero()
	{
		for (byte b : this.array)
		{
			if (b != 0)
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns whether the TetroArray doesn't have any <code>0</code>s.
	 *
	 * @return <code>true</code> if no elements is equal to <code>0</code>,
	 * <code>false</code> otherwise
	 */
	public boolean isFull()
	{
		for (byte b : this.array)
		{
			if (b == 0)
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * Swaps every nonzero <code>byte</code> in the TetroArray with the
	 * <code>byte</code> passed as an argument.
	 *
	 * @param b the <code>byte</code> to set
	 */
	public void setByte(byte b)
	{
		for (int i = 0; i < this.playFieldWidth; ++i)
		{
			if (this.array[i] != 0)
			{
				this.array[i] = b;
			}
		}
	}

	/**
	 * Returns the index of the first nonzero element.
	 *
	 * @return the index
	 */
	public int firstNonZeroElementIndex()
	{
		for (int i = 0; i < this.playFieldWidth; ++i)
		{
			if (this.array[i] != 0)
			{
				return i;
			}
		}

		return -1;
	}

	/**
	 * Returns the index of the last nonzero element.
	 *
	 * @return the index
	 */
	public int lastNonZeroElementIndex()
	{
		for (int i = (this.playFieldWidth - 1); i > 0; --i)
		{
			if (this.array[i] != 0)
			{
				return i;
			}
		}

		return -1;
	}

	/**
	 * Checks equality between two TetroArrays.
	 *
	 * @param comparison the TetroArray to compare
	 *
	 * @return <code>true</code> if the two TetroArrays are exactly the same,
	 * <code>false</code> otherwise
	 */
	public boolean equals(TetroArray comparison)
	{
		return
		(
			(this.playFieldWidth == comparison.playFieldWidth) &&
			Arrays.equals(this.array, comparison.array)
		);
	}

	/**
	 * Converts the TetroArray to a <code>String</code> for debugging purposes.
	 *
	 * @return the <code>String</code> representing the TetroArray.
	 */
	@Override
	public String toString()
	{
		StringBuilder sr = new StringBuilder();
		sr.append("[ ");
		for (byte b: this.array)
		{
			sr.append(b + " ");
		}
		sr.append("]");
		
		return sr.toString();
	}


}
