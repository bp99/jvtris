package eu.bertalanp99.jvtris;

class TetroMatrix {
	/**
	 * An array of TetroArrays that store the bytes.
	 */
	private final TetroArray[] matrix;

	/**
	 * The width of the playfield this TetroMatrix is designed for.
	 */
	private final int playFieldWidth;

	/**
	 * Constructor for a new TetroMatrix.
	 *
	 * @param width the width of the playfield the TetroMatrix is designed for
	 * @param mat the <code>byte</code>s to set as the matrix
	 */
	public TetroMatrix(int width, byte[][] mat)
	{
		if (mat == null)
		{
			throw new NullPointerException();
		}
		else if (! this.check2DByteArrValidity(mat))
		{
			throw new IllegalArgumentException("2-dimensional byte array " +
					"passed must represent a 4x4 square matrix and must not " +
					"have null elements.");
		}

		this.playFieldWidth = width;

		this.matrix = new TetroArray[mat.length];
		for (int i = 0; i < this.matrix.length; ++i)
		{
			this.matrix[i] = new TetroArray(this.playFieldWidth, mat[i]);
		}
	}

	/**
	 * Internal method that checks the input argument <code>mat</code> to the
	 * constructor.
	 *
	 * @param mat the 2D array to check
	 *
	 * @return <code>true</code> if the 2D array represents a square matrix,
	 * <code>false</code> otherwise.
	 */
	private boolean check2DByteArrValidity(byte[][] mat)
	{
		if (mat == null)
		{
			throw new NullPointerException();
		}

		if (mat.length != 4)
		{
			return false;
		}

		for (int i = 0; i < 4; ++i)
		{
			if ((mat[i] == null) || (mat[i].length != 4))
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * Swaps every nonzero element in the matrix with a <code>byte b</code>
	 *
	 * @param b the <code>byte</code> to swap everything with
	 */
	public void setByte(byte b)
	{
		for (int i = 0; i < this.matrix.length; ++i)
		{
			this.matrix[i].setByte(b);
		}
	}

	/**
	 * Returns the <code>n</code>-th row of the matrix.
	 *
	 * @param n the number of row to return
	 *
	 * @return the <code>n</code>-th row (a TetroArray)
	 */
	public TetroArray getNthRow(int n)
	{
		if (n < 0 || n >= 4)
		{
			throw new IndexOutOfBoundsException("Index out of bounds (should " +
					" be at least 0 and less than 4).");
		}

		return this.matrix[n];
		
	}

	/**
	 * Getter for the TetroMatrix's size.
	 *
	 * @return the size
	 */
	public int size()
	{
		return this.playFieldWidth;
	}
}
