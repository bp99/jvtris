package eu.bertalanp99.jvtris;

/**
 * The 'O' shaped Tetromino (a box).
 *
 * @author bertalanp99
 * @version 0.0.1
 * @since 0.0.1
 */
final class O extends Tetromino {
	private static final Colour colour = Colour.YELLOW;

	private static final byte[][] NORTH = new byte[][]
	{
        {1, 1, 0, 0},
        {1, 1, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0}
	};

	private static final byte[][] EAST = NORTH;

	private static final byte[][] SOUTH = NORTH;

	private static final byte[][] WEST = NORTH;

	public O(int width)
	{
		super(colour,
				new TetroMatrix(width, NORTH),
				new TetroMatrix(width, EAST),
				new TetroMatrix(width, SOUTH),
				new TetroMatrix(width, WEST));
	}
}

