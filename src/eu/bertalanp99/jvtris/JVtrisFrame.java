package eu.bertalanp99.jvtris;

import javax.swing.AbstractAction;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Frame that contains a game with all of its elements.
 *
 * @version 1.0
 * @since 1.0
 * @author bertalanp99
 */
final class JVtrisFrame extends JFrame {
	private static final long serialVersionUID = 0L;

	/**
	 * The <code>PlayField</code> this game shall use.
	 */
	private final PlayField playField;

	/**
	 * Constructor for a new <code>JVtrisFrame</code>.
	 *
	 * @param pf the <code>PlayField</code> the game shall use
	 */
	JVtrisFrame(PlayField pf)
	{
		this.playField = pf;

		this.setTitle("jvtris");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(pf);

		this.initMenuBar();
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	/**
	 * Internal method to initialize the menu bar.
	 */
	private void initMenuBar()
	{
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(this.createPlayMenu());
		menuBar.add(this.createStatsMenu());
		menuBar.add(this.createQuitMenu());
		this.setJMenuBar(menuBar);
	}

	/**
	 * Internal method to create the 'Play' menu.
	 *
	 * @return the 'Play' <code>JMenu</code> itself
	 */
	private JMenu createPlayMenu()
	{
		JMenu playMenu = new JMenu("Play");
		playMenu.setMnemonic(KeyEvent.VK_P);

		JMenuItem newGame = new JMenuItem(new AbstractAction("New game") {
			public void actionPerformed(ActionEvent e)
			{
				playField.restart();
			}

			private static final long serialVersionUID = 0L;
		});
		newGame.setAccelerator(KeyStroke.getKeyStroke('r'));
		playMenu.add(newGame);

		return playMenu;
	}

	/**
	 * Internal method to create the 'Quit' menu.
	 *
	 * @return the 'Quit' <code>JMenu</code> itself
	 */
	private JMenu createQuitMenu()
	{
		JMenu quitMenu = new JMenu("Quit");
		quitMenu.setMnemonic(KeyEvent.VK_Q);

		JMenuItem quit = new JMenuItem(new AbstractAction("Quit") {
			public void actionPerformed(ActionEvent e)
			{
				playField.togglePause();
				int response = JOptionPane.showConfirmDialog(null,
						"Are you sure you want to quit?", "Please confirm",
						JOptionPane.YES_NO_OPTION);
				if (response == 0)
				{
					System.exit(0);
				}
				else
				{
					playField.togglePause();
				}
			}

			private static final long serialVersionUID = 0L;
		});
		quit.setAccelerator(KeyStroke.getKeyStroke('q'));
		quitMenu.add(quit);

		return quitMenu;
	}

	/**
	 * Internal method to create the 'Statistics' menu.
	 *
	 * @return the 'Statstics' <code>JMenu</code> itself.
	 */
	private JMenu createStatsMenu()
	{
		JMenu statsMenu = new JMenu("Statistics");
		statsMenu.setMnemonic(KeyEvent.VK_S);

		JMenuItem leaderboard = new JMenuItem(new
				AbstractAction("Leaderboard") {
			public void actionPerformed(ActionEvent e)
			{
				playField.togglePause();
				JFrame leaderBoardsFrame = new JFrame();
				JLabel leaderBoardsText = new JLabel();
				leaderBoardsText.setText("<html>");
				LeaderBoard lb = playField.getLeaderBoard();
				for (int i = 0; i < lb.size(); ++i)
				{
					leaderBoardsText.setText(leaderBoardsText.getText() +
							(i + 1) + ": " + lb.getName(i) + " " +
							lb.getScore(i) + "<br>");
				}
				leaderBoardsText.setText(leaderBoardsText.getText() +
						"</html>");

				leaderBoardsFrame.add(leaderBoardsText);
				leaderBoardsFrame.pack();
				leaderBoardsFrame.setVisible(true);
			}

			private static final long serialVersionUID = 0L;
		});

		statsMenu.add(leaderboard);
		return statsMenu;
	}

	/**
	 * Publically accessible method that starts a new game.
	 */
	public void startGame()
	{
		this.playField.start();
	}

}
