package eu.bertalanp99.jvtris;

/**
 * Class representing the score in a jvtris game.
 *
 * @version 0.1
 * @since 0.1
 * @author bertalanp99
 */
class TetroScore {
	/**
	 * Keeps track of the actual score.
	 */
	private int score = 0;

	/**
	 * Adds <code>n</code> cleared line's worth of points.
	 *
	 * @param n number of lines cleared
	 */
	public void line(int n)
	{
		if (n < 0)
		{
			throw new IllegalArgumentException("Argument must be positive.");
		}

		this.score += (n * 100);
	}

	/**
	 * Adds a cleared Tetris' worth of points.
	 */
	public void tetris()
	{
		this.score += 800;
	}

	/**
	 * Adds <code>n</code> cleared back-to-back Tetris' worth of points.
	 *
	 * @param n
	 */
	public void backToBack(int n)
	{
		if (n < 0)
		{
			throw new IllegalArgumentException("Argument must be positive.");
		}

		this.score += (n * 1200);
	}

	/**
	 * Resets the score to <code>0</code>.
	 */
	public void reset()
	{
		this.score = 0;
	}

	/**
	 * Getter for the <code>score</code>.
	 *
	 * @return the score
	 */
	public int getScore()
	{
		return this.score;
	}

	/**
	 * Returns a <code>String</code> representation of the score for debugging
	 * purposes.
	 *
	 * @return the <code>String</code> representation
	 */
	@Override
	public String toString()
	{
		return Integer.toString(this.score);
	}
}
