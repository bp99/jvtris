package eu.bertalanp99.jvtris;

/**
 * The 'Z' shaped Tetromino.
 *
 * @author bertalanp99
 * @version 0.0.1
 * @since 0.0.1
 */
final class Z extends Tetromino {
	private static final Colour colour = Colour.RED;

	private static final byte[][] NORTH = new byte[][]
	{
        {1, 1, 0, 0},
        {0, 1, 1, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0}
	};

	private static final byte[][] EAST = new byte[][]
	{
        {0, 0, 1, 0},
        {0, 1, 1, 0},
        {0, 1, 0, 0},
        {0, 0, 0, 0}
	};

	private static final byte[][] SOUTH = NORTH;

	private static final byte[][] WEST = EAST;

	public Z(int width)
	{
		super(colour,
				new TetroMatrix(width, NORTH),
				new TetroMatrix(width, EAST),
				new TetroMatrix(width, SOUTH),
				new TetroMatrix(width, WEST));
	}
}
