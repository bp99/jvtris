package eu.bertalanp99.jvtris;

import static com.esotericsoftware.minlog.Log.*;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * Godlike class that has almost all the game logic.
 *
 * @version 0.1
 * @since 0.1
 * @author bertalanp99
 */
class PlayField extends JPanel {
	/**
	 * Constant for the playfield's width.
	 */
	private static final int WIDTH = 10;

	/**
	 * Constant for the playfield's height.
	 */
	private static final int HEIGHT = 20;

	/**
	 * Constant for the game's scale.
	 */
	private static final int SCALE = 30;

	/**
	 * Constant for the initial delay before the game starts.
	 */
	private static final long INITIAL_DELAY = 100; // in ms

	/**
	 * Constant for the initial step interval.
	 */
	private static final long INITIAL_STEP_INTERVAL = 800;

	/**
	 * Constant for multiplying the step interval at each level with.
	 */
	private static final double STEP_INTERVAL_MULTIPLIER = 0.85;

	/**
	 * Constant for the filename to save the leaderboards at.
	 */
	private static final String LEADERBOARD_FILE_NAME = "leaders.ser";

	/**
	 * Enumeration of Tetromino colours.
	 */
	private static enum Colour {
		CYAN   ((byte) 'c', 0,   199, 199), // I
		BLUE   ((byte) 'b', 0,   0,   255), // J
		ORANGE ((byte) 'o', 255, 165, 0  ), // L
		YELLOW ((byte) 'y', 255, 255, 0  ), // O
		GREEN  ((byte) 'g', 0,   255, 0  ), // S
		PURPLE ((byte) 'm', 128, 0,   128), // T
		RED    ((byte) 'r', 255, 0,   0  ); // Z

		/**
		 * Byte representing the colour.
		 */
		private final byte colourCode;

		/**
		 * Colour to render the colour as. ???
		 */
		private final java.awt.Color colour;

		/**
		 * Unavailable constructor for a Colour.
		 *
		 * @param code the byte representing the colour
		 * @param red the red component
		 * @param green the green component
		 * @param blue the blue component
		 */
		private Colour(byte code, int red, int green, int blue)
		{
			this.colourCode = code;
			this.colour = new java.awt.Color(red, green, blue);
		}

		/**
		 * Getter for the byte representing the colour.
		 *
		 * @return the byte that represents the colour
		 */
		public byte getColourCode()
		{
			return this.colourCode;
		}

		/**
		 * Getter for the colour to render this colour as.
		 *
		 * @return the colour to render
		 */
		public java.awt.Color getColour()
		{
			return this.colour;
		}

		/**
		 * Static method to get the colour to render a byte representing one as.
		 *
		 * @param code the byte representing the colour
		 *
		 * @return what to render the colour as
		 */
		public static java.awt.Color getColourFromCode(byte code)
		{
			for (Colour c: values())
			{
				if (c.colourCode == code)
				{
					return c.getColour();
				}
			}

			return java.awt.Color.BLACK;
		}
	}

	/**
	 * Keeps track of how often to cycle the game (descend tetromino).
	 */
	private long stepInterval;

	private static final long serialVersionUID = 0L;

	/**
	 * The set of tetrominoes that can appear on this playfield.
	 */
	private Set<Tetromino> tetrominoes = new HashSet<Tetromino>();

	/**
	 * The current score.
	 */
	private TetroScore score;

	/**
	 * The leaderboards.
	 */
	private LeaderBoard leaderBoard;

	/**
	 * The number of cleared lines in this game.
	 */
	private int clearedLines;

	/**
	 * The current game stage.
	 */
	private int level;

	/**
	 * Essentially a timer that does a tick on a scheduled basis.
	 */
	private ScheduledExecutorService executor;

	/**
	 * Keeps track of whether the game is paused.
	 */
	private boolean paused;

	/**
	 * A matrix representing the playfield before the active tetromino entered.
	 */
	private TetroArray[] staticMatrix;

	/**
	 * The current state of the playfield (what the player sees).
	 */
	private TetroArray[] activeMatrix;

	/**
	 * The currently active tetromino.
	 */
	private Tetromino activeTetromino;

	/**
	 * Helper pointer to the next row.
	 */
	private int cursor;

	/**
	 * Keeps track of the shift on the width axis.
	 */
	private int shift;

	/**
	 * Constructor for a new playfield.
	 */
	PlayField()
	{
		this.setLayout(new GridBagLayout());
		this.initialize();
		this.initControls();
	}

	/**
	 * Initializes the playfield.
	 */
	private void initialize()
	{
		debug("PlayField", "Initializing variables.");
		this.clearedLines = 0;
		this.level = 0;
		this.paused = false;
		this.cursor = 0;
		this.shift = 0;
		this.stepInterval = INITIAL_STEP_INTERVAL;

		this.score = new TetroScore();

		this.initLeaderBoard();

		debug("PlayField", "Setting up new playfield w/ dimensions " + WIDTH +
				"x" + HEIGHT + ".");
		this.activeMatrix = new TetroArray[HEIGHT];
		for (int i = 0; i < HEIGHT; ++i)
		{
			this.activeMatrix[i] = new TetroArray(WIDTH);
		}

		this.backupMatrix();

		this.resetExecutor();
		this.startExecutor();
	}

	/**
	 * Initializes the leaderboard.
	 */
	private void initLeaderBoard()
	{
		try
		{
			FileInputStream fis = new FileInputStream(LEADERBOARD_FILE_NAME);
			ObjectInputStream ois = new ObjectInputStream(fis);
			this.leaderBoard = (LeaderBoard) ois.readObject();
		}
		catch (FileNotFoundException fnfe)
		{
			this.leaderBoard = new LeaderBoard();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	/**
	 * Creates a copy of the active matrix into the static one.
	 */
	private void backupMatrix()
	{
		debug("PlayField", "Copying active playfield matrix into static.");
		this.staticMatrix = new TetroArray[HEIGHT];
		for (int i = 0; i < HEIGHT; ++i)
		{
			this.staticMatrix[i] = new TetroArray(this.activeMatrix[i]);
		}
	}

	/**
	 * Restores the active matrix using the static one.
	 */
	private void restoreMatrix()
	{
		debug("PlayField", "Restoring active playfield matrix from static.");
		this.activeMatrix = new TetroArray[HEIGHT];
		for (int i = 0; i < HEIGHT; ++i)
		{
			this.activeMatrix[i] = new TetroArray(this.staticMatrix[i]);
		}
	}

	/**
	 * Pauses and unpauses the game on demand.
	 */
	public void togglePause()
	{
		if (this.paused)
		{
			this.resetExecutor();
			this.startExecutor();
			this.paused = false;
			info("GAME RESUMED.");
		}
		else
		{
			this.stopExecutor();
			this.paused = true;
			info("GAME PAUSED.");
		}
	}

	/**
	 * Getter for the current score.
	 *
	 * @return the current score
	 */
	public String getScore()
	{
		return this.score.toString();
	}

	/**
	 * Initializes the game controls.
	 */
	private void initControls()
	{
		debug("PlayField", "Initializing controls using KeyAdapter.");
		KeyAdapter ka = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent ke)
			{
				trace("KeyAdapter", "Key pressed event!");
				if (paused)
				{
					if (ke.getKeyCode() == KeyEvent.VK_P)
					{
						trace("KeyAdapter", "'P' key pressed.");
						togglePause();
					}

					return;
				}

				switch (ke.getKeyCode())
				{
					case KeyEvent.VK_P:
						trace("KeyAdapter", "'P' key pressed.");
						togglePause();
						break;

					case KeyEvent.VK_LEFT:
						trace("KeyAdapter", "Keypad left key pressed.");
						moveLeft();
						break;

					case KeyEvent.VK_RIGHT:
						trace("KeyAdapter", "Keypad right key pressed.");
						moveRight();
						break;

					case KeyEvent.VK_UP:
						trace("KeyAdapter", "Keypad up key pressed.");
						rotate();
						break;

					case KeyEvent.VK_DOWN:
						trace("KeyAdapter", "Keypad down key pressed.");
						step();
						break;

					case KeyEvent.VK_SPACE:
						trace("KeyAdapter", "Spacebar pressed.");
						drop();
						break;
				}

			}
		};

		this.setFocusable(true);
		this.addKeyListener(ka);
	}

	/**
	 * Sets the level based on the number of lines cleared.
	 */
	public void setLevel()
	{
		trace("PlayField", "Calculating level after " + this.clearedLines +
				" cleared lines.");
		this.level = (this.clearedLines / 10);
		debug("PlayField", "Set level to " + this.level);
	}

	/**
	 * Sets the game pace based on the level.
	 */
	public void setSpeed()
	{
		this.stepInterval = INITIAL_STEP_INTERVAL;

		if (this.level != 0)
		{
			for (int i = 0; i < this.level; ++i)
			{
				this.stepInterval *= STEP_INTERVAL_MULTIPLIER;
			}
		}
		debug("PlayField", "Set speed to " + this.stepInterval);

		this.stopExecutor();
		this.resetExecutor();
		this.startExecutor();
	}

	/**
	 * Starts the game.
	 */
	public void start()
	{
		debug("PlayField", "Starting game.");
		this.next();
		this.togglePause();
	}

	/**
	 * Stops the game and initializes a new one.
	 */
	public void restart()
	{
		debug("PlayField", "Restarting game.");
		this.stopExecutor();
		this.initialize();
		this.start();
	}

	/**
	 * Restarts the timer.
	 */
	private void resetExecutor()
	{
		trace("PlayField", "Resetting step executor.");
		this.executor = Executors.newSingleThreadScheduledExecutor();
	}

	/**
	 * Does one game step.
	 */
	private final Runnable executeStep = new Runnable() {
		public void run() { trace("PlayField", "Stepping."); step(); }
	};

	/**
	 * Starts the timer.
	 */
	private void startExecutor()
	{
		trace("PlayField", "Starting step executor.");
		this.executor.scheduleAtFixedRate(this.executeStep, INITIAL_DELAY,
				this.stepInterval, TimeUnit.MILLISECONDS);
	}

	/**
	 * Stops the timer.
	 */
	private void stopExecutor()
	{
		trace("PlayField", "Halting step executor.");
		this.executor.shutdownNow();
	}

	/**
	 * Adds a tetromnio shape to the game.
	 *
	 * @param t the tetromino to add
	 */
	public void addTetromino(Tetromino t)
	{
		debug("PlayField", "Adding a new tetromino to the playfield.");
		trace("PlayField", "The tetromino's hash is " + t.hashCode());
		this.tetrominoes.add(t);
	}

	/**
	 * Sets the currently active tetromino.
	 *
	 * @param t what tetromino to set
	 */
	private void setActiveTetromino(Tetromino t)
	{
		debug("PlayField", "Setting a new active tetromino.");
		this.activeTetromino = t;
	}

	/**
	 * Randomly selects a new tetromino from the set.
	 */
	void generateNextTetromino()
	{
		// get a  random tetromino from the tetromino set
		debug("PlayField", "Generating a new random tetromiono.");
		int r = new Random().nextInt(this.tetrominoes.size());
		int i = 0;
		for (Tetromino t : this.tetrominoes)
		{
			if (i++ == r)
			{
				this.setActiveTetromino(t);
			}

		}
	}

	/**
	 * Checks if the cursor is at the bottom of the playfield.
	 *
	 * @return <code>true</code> if at bottom, <code>false</code> otherwise
	 */
	private boolean atBottom()
	{
		return (this.cursor == HEIGHT);
	}

	/**
	 * Checks if the cursor is at the top of the playfield.
	 *
	 * @return <code>true</code> if at top, <code>false</code> otherwise
	 */
	private boolean atTop()
	{
		return (this.cursor == 0);
	}

	/**
	 * Checks if the active tetromino has something supporting it from below.
	 *
	 * @return <code>true</code> if it has support, <code>false</code> otherwise
	 */
	private boolean supported()
	{
		if (this.atBottom())
		{
			trace("PlayField", "At bottom of field.");
			return true;
		}

		this.activeTetromino.resetRow();
		int runCursor = this.cursor;
		while (runCursor >= 0)
		{
			TetroArray next = this.activeTetromino.nextRow();
			if (next == null)
			{
				break;
			}

			next = next.shift(this.shift);
			trace("PlayField", "Attempting to merge " + next.toString() +
					" with " + this.staticMatrix[runCursor].toString() + ".");
			if (! next.isMergeableWith(this.staticMatrix[runCursor]))
			{
				trace("PlayField", "Unable to merge.");
				return true;
			}

			--runCursor;
		}

		debug("PlayField", "Tetromino is not supported from below.");
		return false;
	}

	/**
	 * Checks if the active tetromino touches something on the left.
	 *
	 * @return <code>true</code> if it does, <code>false</code> otherwise.
	 */
	private boolean collidesOnLeft()
	{
		this.activeTetromino.resetRow();
		int runCursor = (this.cursor - 1);
		while (runCursor >= 0)
		{
			TetroArray next = this.activeTetromino.nextRow();
			if (next == null)
			{
				break;
			}

			next = next.shift(this.shift);

			if (! next.isLeftPadded())
			{
				return true;
			}

			TetroArray potentialNext = next.shiftLeft();
			trace("PlayField", "Attempting to merge " +
					potentialNext.toString() + " with " +
					this.staticMatrix[runCursor].toString() + ".");
			if (! potentialNext.isMergeableWith(this.staticMatrix[runCursor]))
			{
				trace("PlayField", "Unable to merge.");
				return true;
			}

			--runCursor;
		}

		debug("PlayField", "Tetromino does not collide on left.");
		return false;
	}

	/**
	 * Checks if the active tetromino touches something on the right.
	 *
	 * @return <code>true</code> if it does, <code>false</code> otherwise.
	 */
	private boolean collidesOnRight()
	{
		this.activeTetromino.resetRow();
		int runCursor = (this.cursor - 1);
		while (runCursor >= 0)
		{
			TetroArray next = this.activeTetromino.nextRow();
			if (next == null)
			{
				break;
			}

			next = next.shift(this.shift);
			if (! next.isRightPadded())
			{
				return true;
			}

			TetroArray potentialNext = next.shiftRight();
			trace("PlayField", "Attempting to merge " +
					potentialNext.toString() + " with " +
					this.staticMatrix[runCursor].toString() + ".");
			if (! potentialNext.isMergeableWith(this.staticMatrix[runCursor]))
			{
				trace("PlayField", "Unable to merge.");
				return true;
			}

			--runCursor;
		}

		debug("PlayField", "Tetromino does not collide on right.");
		return false;
	}

	/**
	 * Checks if the active Tetromino would fit into the playfield after a
	 * rotation.
	 *
	 * @return <code>true</code> if so, <code>false</code> otherwise
	 */
	private boolean canRotate()
	{
		this.activeTetromino.resetRow();
		this.activeTetromino.rotateCW();
		int runCursor = this.atBottom() ? (HEIGHT - 1) : this.cursor;
		while (runCursor >= 0)
		{
			TetroArray next = this.activeTetromino.nextRow();
			if (next == null)
			{
				break;
			}

			next = next.shift(this.shift);
			if (next == null)
			{
				this.activeTetromino.rotateCCW();
				return false;
			}

			if (! next.isMergeableWith(this.staticMatrix[runCursor]))
			{
				this.activeTetromino.rotateCCW();
				return false;
			}

			--runCursor;
		}

		debug("PlayField", "Tetromino cannot be rotated.");
		this.activeTetromino.rotateCCW();
		return true;
	}

	/**
	 * Internal method that restores the <code>n</code>-th row of the static
	 * playfield matrix.
	 *
	 * @param n the number of row to restore, must be between <code>0</code and
	 * <code>WIDTH</code>
	 */
	private void restoreNthRow(int n)
	{
		trace("PlayField", "Restoring the " + n +
				"-th row of the active matrix.");
		this.activeMatrix[n] = new TetroArray(this.staticMatrix[n]);
	}

	/**
	 * Internal method to be called whenever a tetromino has finished falling
	 * and the next one should be spawned.
	 */
	private void next()
	{
		debug("PlayField", "Getting the next tetromino on the playfield.");
		this.clearLines();
		this.setLevel();
		this.setSpeed();

		if (this.activeTetromino != null)
		{
			this.activeTetromino.reset();
		}

		this.backupMatrix();
		this.generateNextTetromino();
		this.spawn();
	}

	/**
	 * Internal method that spawns a new tetromino onto the playfield.
	 */
	private void spawn()
	{
		debug("PlayField", "Spawning tetromino.");

		// count tetromino's rows
		int count = 0;
		this.activeTetromino.resetRow();
		TetroArray row = this.activeTetromino.nextRow();
		while (row != null)
		{
			++count;
			row = this.activeTetromino.nextRow();
		}

		// set cursor accordingly
		this.cursor = count;
		this.shift = 0;

		// place tetormino on playfield
		this.activeTetromino.resetRow();
		int runCursor = (this.cursor - 1);
		while (runCursor >= 0)
		{
			TetroArray next = this.activeTetromino.nextRow();
			if (next == null)
			{
				break;
			}

			trace("PlayField", "Attempting to merge " + next.toString() +
					" with " + this.staticMatrix[runCursor].toString() + ".");
			if (! next.isMergeableWith(this.staticMatrix[runCursor]))
			{
				trace("PlayField", "Unable to merge.");
				this.gameOver();
				return;
			}

			trace("PlayField", "Placing tetromino on playfield");
			TetroArray merged = this.activeMatrix[runCursor].mergeWith(next);
			this.activeMatrix[runCursor] = merged;

			--runCursor;
		}

		this.repaint();
	}

	/**
	 * Internal method that performs one game step (descends the active
	 * tetromino by <code>1</code).
	 */
	private void step()
	{
		debug("PlayField", "Stepping.");
		if (this.supported())
		{
			trace("PlayField", "Tetromino has support, calling the next one.");
			this.next();
		}
		else
		{
			this.descend();
			++this.cursor;
		}
	}

	/**
	 * Internal method that redraws the playfield.
	 *
	 * This is what makes changes visisble to the player.
	 */
	private void redraw()
	{
		trace("PlayField", "Redrawing tetromino in place.");
		this.restoreMatrix();
		this.activeTetromino.resetRow();

		int runCursor = (this.cursor - 1);
		while (runCursor >= 0)
		{
			TetroArray next = this.activeTetromino.nextRow();
			if (next == null)
			{
				break;
			}

			next = next.shift(this.shift);
			TetroArray merged = this.activeMatrix[runCursor].mergeWith(next);
			this.activeMatrix[runCursor] = merged;

			--runCursor;
		}

		this.repaint();
	}

	/**
	 * Internal method that sends the active tetromino down by <code>1</code>
	 * row.
	 */
	private void descend()
	{
		trace("PlayField", "Descending and drawing tetromino.");
		this.restoreMatrix();
		this.activeTetromino.resetRow();

		int runCursor = this.cursor;
		while (runCursor != -1)
		{
			TetroArray next = this.activeTetromino.nextRow();
			if (next == null)
			{
				break;
			}

			next = next.shift(this.shift);
			TetroArray merged = this.activeMatrix[runCursor].mergeWith(next);
			this.activeMatrix[runCursor] = merged;

			--runCursor;
		}

		this.repaint();
	}

	/**
	 * Internal method that clears the <code>n</code>-th line of the playfield.
	 *
	 * @param n number of line to clear
	 */
	private void clearLine(int n)
	{
		debug("PlayField", "Clearing the " + n + "-th line of the playfield.");
		if (n < 0 || n >= HEIGHT)
		{
			throw new IndexOutOfBoundsException();
		}

		this.activeMatrix[n].setByte((byte) 0);
		for (int i = n; i > 0; --i)
		{
			this.activeMatrix[i] = this.activeMatrix[i - 1];
		}

		++this.clearedLines;
	}

	/**
	 * Internal method that finds all full lines and clears them.
	 */
	private void clearLines()
	{
		debug("PlayField", "Checking for lines to clear.");
		for (int i = (HEIGHT - 1); i > 0; --i)
		{
			int count = 0;
			while(this.activeMatrix[i].isFull())
			{
				this.clearLine(i);
				++count;
			}

			if (count >= 4)
			{
				this.score.tetris(); // first Tetris
				this.score.backToBack((count / 4) - 1); // subsequent Tetrises
				this.score.line(count - (count / 4)); // remaining lineclears
			}
			else
			{
				this.score.line(count);
			}
		}
	}

	/**
	 * Internal method that drops the active tetromino (sends it to bottom).
	 */
	private void drop()
	{
		trace("PlayField", "Attempting to drop tetromino.");
		while (! this.supported())
		{
			this.step();
		}

		this.next();
	}

	/**
	 * Internal method that attempts to move the tetromino left on the
	 * playfield.
	 */
	private void moveLeft()
	{
		trace("PlayField", "Attempting to move tetromino left.");
		if (! this.collidesOnLeft())
		{
			--this.shift;
			this.redraw();
		}
	}

	/**
	 * Internal method that attempts to move the tetormino right on the
	 * playfield.
	 */
	private void moveRight()
	{
		trace("PlayField", "Attempting to move tetromino right.");
		if (! this.collidesOnRight())
		{
			++this.shift;
			this.redraw();
		}
	}

	/**
	 * Internal method that attempts to rotate the active tetromino.
	 */
	private void rotate()
	{
		trace("PlayField", "Attempting to rotate tetromino (clockwise).");
		if (this.canRotate())
		{
			this.activeTetromino.rotateCW();
			this.redraw();
		}
	}

	/**
	 * Internal method that should be called when the player fails and the game
	 * is over.
	 */
	private void gameOver()
	{
		this.stopExecutor();
		info("GAME OVER.");
		this.askForHighScore();
	}

	/**
	 * Internal method that prompts the player for a name for the leaderboards.
	 */
	private void askForHighScore()
	{
		String player = JOptionPane.showInputDialog("Please enter your name " +
				"to add yourself to the leaderboard, or leave emtpy if you " +
				"would rather stay anonymous");

		this.leaderBoard.addScore(player.equals("") ? "Anonymous" : player,
				this.score);
		this.saveLeaderBoard();
	}

	/**
	 * Internal method that does persistency to the leaderboards.
	 */
	private void saveLeaderBoard()
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(LEADERBOARD_FILE_NAME);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this.leaderBoard);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Getter for the leaderboard of this playfield.
	 */
	public LeaderBoard getLeaderBoard()
	{
		return this.leaderBoard;
	}

	/**
	 * Internal method that paints the active matrix onto a
	 * <code>Graphics</code> object <code>g</code>.
	 *
	 * @param g the <code>Graphics</code> object to draw on
	 */
	private void paintMatrix(Graphics g)
	{
		for (int j = 0; j < WIDTH; ++j)
		{
			for (int i = 0; i < HEIGHT; ++i)
			{
				byte tile = this.activeMatrix[i].getNth(j);
				int J = (j * SCALE);
				int I = (i * SCALE);
				if (tile != 0)
				{
					g.setColor(Colour.getColourFromCode(tile));
					g.fillRect((J + 1), (I + 1),
							(SCALE - 2), (SCALE - 2));

					g.setColor(Colour.getColourFromCode(tile).brighter());
					g.drawLine(J, (I + SCALE - 1),
							J, I);
					g.drawLine(J, I,
							(J + SCALE - 1), I);

					g.setColor(Colour.getColourFromCode(tile).darker());
					g.drawLine((J + 1), (I + SCALE - 1),
							(J + SCALE - 1), (I + SCALE - 1));
					g.drawLine((J + SCALE - 1), (I + SCALE - 1),
							(J + SCALE - 1), (I + 1));
				}
				else
				{
					g.setColor(java.awt.Color.GRAY);
					g.fillRect((J + SCALE / 2), (I + SCALE / 2), 1, 1);
				}
			}
		}

	}

	/**
	 * Internal method that paints the current score onto the a
	 * <code>Graphics</code> object <code>g</code>.
	 *
	 * @param g the <code>Graphics</code> object to draw on
	 */
	private void drawScore(Graphics g)
	{
		int width = (WIDTH * SCALE);
		g.setColor(java.awt.Color.BLACK);
		g.drawString(("SCORE: " + this.score.toString()),
				(width + SCALE), SCALE);
	}

	/**
	 * Internal method that paints the current level onto a <code>Graphics</code>
	 * object <code>g</code>.
	 *
	 * @param g the <code>Graphics</code> object to draw on
	 */
	private void drawLevel(Graphics g)
	{
		int width = (WIDTH * SCALE);
		g.setColor(java.awt.Color.BLACK);
		g.drawString(("LEVEL: " + this.level), (width + SCALE), (2 * SCALE));
	}

	/**
	 * Internal method that paints the number of cleared lines so far onto a
	 * <code>Graphics</code> object <code>g</code>.
	 *
	 * @param g the <code>Graphics</code> object to draw on
	 */
	private void drawClearedLines(Graphics g)
	{
		int width = (WIDTH * SCALE);
		g.setColor(java.awt.Color.BLACK);
		g.drawString(("LINES: " + this.clearedLines),
				(width + SCALE), (4 * SCALE));
	}

	/**
	 * Internal method that shows whether the game is paused on a
	 * <code>Graphics</code> object <code>g</code>.
	 *
	 * @param g the <code>Graphics</code> object to draw on
	 */
	private void drawPause(Graphics g)
	{
		int width = (WIDTH * SCALE);
		g.setColor(java.awt.Color.BLACK);
		if (this.paused)
		{
			g.drawString("PAUSED", (width + SCALE), (8 * SCALE));
		}

	}

	/**
	 * Returns the preferred dimensions of this <code>JPane</code>.
	 *
	 * @return the preferred dimensions
	 */
	@Override
	public Dimension getPreferredSize()
	{
		return new Dimension(((WIDTH + 5) * SCALE), (HEIGHT * SCALE));
	}

	/**
	 * Paints everything needed on a <code>Graphics</code> object <code>g</code>
	 *
	 * @param g the <code>Graphics</code> object to paint on.
	 */
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		g.drawRect(0, 0, (WIDTH * SCALE), (HEIGHT * SCALE));

		this.paintMatrix(g);

		this.drawScore(g);
		this.drawLevel(g);
		this.drawClearedLines(g);
		this.drawPause(g);
	}
}
